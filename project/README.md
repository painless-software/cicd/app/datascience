# {{project}}

{{description}}.
Jupyter notebooks, queries, reports and library source code.

This project contains the machine learning source code and builds the
Python package `{{package}}`. See the [Releases][releases]
section in the contribution guide for details.

## Getting Started

See [CONTRIBUTING][contrib] for instructions on development and testing.

[releases]: CONTRIBUTING.md#releases
[contrib]: CONTRIBUTING.md
