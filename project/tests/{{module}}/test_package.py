"""
Tests for Python package ``{{package}}``.
"""


def test_package():
    """Can the main module (of the installed package) be imported?"""
    expected = "{{project}}"

    import {{module}}

    assert {{module}}.__doc__ == expected
