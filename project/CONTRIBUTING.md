# Contributing

... (general instructions on how to get started) ...

## Prerequisites

Install [Tox][tox] on your development machine, e.g.

```shell
python3 -m pip install tox
```

For Tox to be able to build the project you will need some system
dependencies in addition, e.g. on Ubuntu distros:

```shell
sudo apt-get install python3-venv
```

To work with ODBC you need [system dependencies for pyodbc][pyodbc]
installed on your developer machine, e.g. on Ubuntu distros:

```shell
sudo apt-get install unixodbc
```

## Local Development

If you need to work interactively you can use [Tox][tox] to provision a
virtual environment with the dependencies it uses for running tests, e.g.

```shell
tox devenv                 # create virtual environment `venv`
source venv/bin/activate   # activate virtual environment
pip install -e '.[dev]'    # editable install with dev dependencies
```

You can now run any Python, Jupyter or test command.

To leave the virtual environment, execute:

```shell
deactivate
```

## Testing

This project uses [Tox][tox] for managing test environments. Use the
`tox` command in the terminal to run your tests and checks locally:

```shell
tox list                 # list available environments
tox run -e lint,format   # run a few environments
tox run -e py            # run tests with local default Python
tox                      # run entire suite
```

If you need to use command arguments for a command executed by a Tox
environment separate them from the Tox command with a double-dash, e.g.

```shell
tox run -e py -- -vv --exitfirst
tox run -e lint -- . --fix
tox run -e format -- --help
```

## Development Process

### Branching Model

This project implements a process with simple, powerful topic branching
and optional review apps. You're encouraged to use `main` as your default
branch.

1. Start a topic branch (e.g. `feature/awesome-thing`, `fix/query-bug`).
1. Make code changes and add related tests.
1. Open a merge request (MR). GitLab will [mark test coverage results](
   https://docs.gitlab.com/ee/ci/testing/test_coverage_visualization.html)
   in the file diff view.
1. The CI/CD pipeline deploys a [review app][review-app] in your dev/test
   environment to allow hands-on verification. 🚀 (optional)
1. Merge the MR after code review, which will tear down the review app.

### Releases

For deployments to production you're encouraged to release a stable
version, preferably using [SemVer][semver] or [CalVer][calver] as a
versioning scheme.

1. Bump the [package version][package-version] and
   [create a release with a new tag][gitlab-releases] matching the
   package version.
1. The CI/CD pipeline will build a Python package and push it to the
   [local package registry][gitlab-packages]. 🚀

[tox]: https://tox.wiki/
[pyodbc]: https://github.com/mkleehammer/pyodbc/wiki/Install
[review-app]: https://about.gitlab.com/stages-devops-lifecycle/review-apps/
[semver]: https://semver.org/
[calver]: https://calver.org/
[package-version]: pyproject.toml#L7
[gitlab-releases]: {{url}}/{{package}}/-/releases
[gitlab-packages]: {{url}}/{{package}}/-/packages
