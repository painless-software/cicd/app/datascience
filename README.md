# Painless CI/CD Data Science Project

A Copier template for data science development with modern CI/CD.

Run this template over your existing data science project to add testing,
packaging and CI/CD. See the [Cookiecutter Data Science][cookiecutter]
for a common layout.

## Usage

```console
pip install copier
```

```console
copier copy gl:painless-software/cicd/app/datascience path/to/your/new/project
```

Once you have put your setup under version control you can pull in changes
from this Copier template using the `update` command:

```console
copier update
```

## Development

See [CONTRIBUTING](CONTRIBUTING.md).

## Background Reading

Some resources that inspired this setup:

- [Cookiecutter Data Science][cookiecutter] (GitHub Pages)

[cookiecutter]: https://drivendata.github.io/cookiecutter-data-science/
